import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ItemShoe from "./ItemShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {

  // componentDidMount ~ life cycle
  render() {
    return (
      <div className="container py-5">
        <Cart/>
        {/* <div className="row">{this.renderListShoe()}</div> */}
        <ListShoe
        />
        <DetailShoe/>
      </div>
    );
  }
}


// let color= ["blue","red"]
// color.pus("white")

// let newColors=[...color,"white"]
