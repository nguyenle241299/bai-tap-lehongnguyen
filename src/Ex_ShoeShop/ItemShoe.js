import React, { Component } from "react";
import { connect } from 'react-redux';

class ItemShoe extends Component {
  render() {
    console.log(this.props);
    let { image, name, price } = this.props.data;
    return (
      <div className="col-3 p-1">
        <div className="card text-left h-100">
          <img className="card-img-top" src={image} alt />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <h2 className="card-title">{price} $</h2>
          </div>
          <div>
            <button
              onClick={() => {
                this.props.handleClick(this.props.data);
              }}
              className="btn btn-secondary"
            >
              Xem chi tiết
            </button>
            <button
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
              className="btn btn-danger"
            >
              Chọn mua
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapDisPatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      let action = {
        type: "ADD_TO_CART",
        payload: shoe,
      }
      dispatch(action);
    },
    
    handleClick: (shoe) => {
      let action = {
        type: "XEM_CHI_TIET",
        payload: shoe,
      }
      dispatch(action);
    }
  }
}

export default  connect(null, mapDisPatchToProps)(ItemShoe);
// does not match
// can not find moudle
