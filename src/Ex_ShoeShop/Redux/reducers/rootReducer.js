import { combineReducers } from "redux";
import { shoeReducer } from './shoeShop_Reducer';

export const rootReducer_ShoeShop = combineReducers({shoeReducer})