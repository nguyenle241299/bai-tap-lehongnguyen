import { dataShoe } from './../../dataShoe';
let initialState = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
}

export const shoeReducer = (state = initialState, action) => {
    switch(action.type) {
        case "ADD_TO_CART" : {
            let cloneCart = [...state.cart]
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.id;
            })

            if(index == -1) {
                let cartItem = {...action.payload, number: 1};
                cloneCart.push(cartItem);
            } else {
                cloneCart[index].number++;
            }

            return {...state, cart: cloneCart};
        }

        case "TANG_GIAM_SO_LUONG": {
            let cloneCart = [...state.cart];
            let index = cloneCart.findIndex((item) => {
                return item.id == action.payload.idShoe;
            });

            if(index !== -1) {
                cloneCart[index].number += action.payload.soLuong;
            }

            if(cloneCart[index].number == 0) {
                cloneCart.splice(index, 1);
            }
            return {...state, cart: cloneCart};
        }

        case "XEM_CHI_TIET": {
            console.log(action.payload);
            return {...state, detail: action.payload};
        }
        default:
            return state;
    }
}